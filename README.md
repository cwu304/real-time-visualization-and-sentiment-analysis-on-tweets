# README #

Gathered some real-time tweets against the given key word via the Twitter streaming API, using MongoDB fordata storage, using Python for natural language processing

Visualized the data to show people’s reaction on a spatial and temporal basis, with several method (Calendar View Heatmap, Map View etc). Added interactive features to enable users choose location and time period.